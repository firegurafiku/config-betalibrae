; Auxiliary function.
(defun add-list-to-list (list-var &rest list)
    (dolist (item list)
        (add-to-list list-var item t)))

; Add additional package repositories.
(require 'package)
(add-list-to-list 'package-archives
    '("marmalade" . "http://marmalade-repo.org/packages/")
    '("melpa"     . "http://melpa.org/packages/"))

; Initialize packages installed with ELPA. In particular, this
; line is essential for (load-theme ...) statements below.
(package-initialize)

; Disable nasty GUI.
(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)
(column-number-mode t)

; Basic text appearance paramters.
(set-default-font "DejaVu Sans Mono 11")
(load-theme 'solarized-dark t)
(load-theme 'solarized-light t)

(setq c-default-style "java"
      c-basic-offset 4)

; Come to the dark side.
(evil-mode)
