(autoload 'wl "wl" "Wanderlust" t)

;; Default folder name for auto completion in message refile dialog.
(setq wl-default-spec "%")

;; Folder to save drafts to. I save drafts locally as my networking suddenly
;; gets down sometimes.
(setq wl-draft-folder "+drafts")

;; Mark sent messages (folder carbon copy) as read.
(setq wl-fcc-force-as-read t)

;; Automatically safe drafts every 30 seconds (set to 'nil to disable).
(setq wl-auto-save-drafts-interval 30)
  
;; Let the SMTP servers handle the message-id and make Wanderlust stop warning.
(setq wl-insert-message-id nil)

;; I don't know why I need this.
(setq elmo-imap4-use-modified-utf7 t)

; Use "Fwd: " instead of "Forward: "
(setq wl-forward-subject-prefix "Fwd: ")

; Setting as "t" means that wanderlust should use a new frame for the draft
(setq wl-draft-use-frame t)
  
; Proportion of the summary and message windows
(setq wl-message-window-size '(3 . 7))

(setq wl-summary-line-format "%n%T%P %W %Y-%M-%D %h:%m %t %[%25(%c %f%) %] %s")
(setq wl-summary-width 150)

; === Settings for multiple accounts to work ===

; Kinda primary (?) email address. I don't know why it is necessary, but without
; this Emacs suggest something like username@hostname and issues a warning.
(setq user-mail-address "firegurafiku@gmail.com")

; List of email addresses which belong to me. These values are used
; by Wanderlust to detect (and highlight?) messages sent by me.
(setq wl-user-mail-address-list '(
    "kretov@phys.vsu.ru"
    "kretovpa@gmail.com"
    "firegurafiku@gmail.com"))

;; The following is a list of *templates*. Wanderlust doesn't have a concept of
;; multiple accounts, instead in allow to automatically adjust message settings
;; base on certain criteria. These settings are grouped into templates.
(setq wl-template-alist '(
    ;; Settings for my GMail account.
    ("gmail"
       (wl-from                   . "Pavel Kretov <firegurafiku@gmail.com>")
       (wl-smtp-connection-type   . 'starttls)
       (wl-smtp-posting-port      . 587)
       (wl-smtp-authenticate-type . "plain")
       (wl-smtp-posting-user      . "firegurafiku@gmail.com")
       (wl-smtp-posting-server    . "smtp.gmail.com")
       (wl-local-domain           . "gmail.com")
       ("From"                    . wl-from))

    ;; Settings for my university account.
    ("vsu"
       (wl-from                   . "Pavel Kretov <kretov@phys.vsu.ru>")
       (wl-smtp-connection-type   . 'ssl)
       (wl-smtp-posting-port      . 465)
       (wl-smtp-authenticate-type . "login")
       (wl-smtp-posting-user      . "kretov@phys.vsu.ru")
       (wl-smtp-posting-server    . "info.vsu.ru")
       (wl-local-domain           . "phys.vsu.ru")
       ("From"                    . wl-from)
       ;; Directory for Wanderlust to copy message after sending. My university
       ;; server, unlike GMail's, does not put sent messages into "Sent" folder
       ;; automatically.
       ("Fcc" . "%Sent:\"kretov@phys.vsu.ru\"/digest-md5@info.vsu.ru:993!"))))
  
;; This is the list of rules for choosing message template.
;; 
(setq wl-draft-config-alist '(
    ;; If I reply a message on university server, apply template "vsu".
    ((string-match ".*phys.vsu" wl-draft-parent-folder)
     (template . "vsu"))
    
    ;; If I open a draft message, and its "From:" field points to my university
    ;; main, then apply template "vsu".
    ("From: .*kretov@phys\\.vsu\\.ru.*"
     (template . "vsu"))
    
    ;; Otherwise apply template "gmail" as a failsafe.
    (t (template . "gmail"))))

;; Apply only the first matched rule of 'wl-draft-config-alist'.
(setq wl-draft-config-matchone t)

;; Apply 'wl-draft-config-alist' as soon as you enter a draft buffer. Without
;; this Wanderlust will apply it only when actually sending the e-mail.
(add-hook 'wl-mail-setup-hook 'wl-draft-config-exec)

; If you want to manually change the template use C-c C-j in a draft buffer
; (wl-template-select). The four lines below allow changint the template with
; the arrow keys
;(define-key wl-template-mode-map (kbd "<right>") 'wl-template-next)
;(define-key wl-template-mode-map (kbd "<left>") 'wl-template-prev)
;(define-key wl-template-mode-map (kbd "<up>") 'wl-template-next)
;(define-key wl-template-mode-map (kbd "<down>") 'wl-template-prev)

(autoload 'wl-user-agent-compose "wl-draft" nil t)
(if (boundp 'mail-user-agent)
    (setq mail-user-agent 'wl-user-agent))
(if (fboundp 'define-mail-user-agent)
    (define-mail-user-agent
        'wl-user-agent
        'wl-user-agent-compose
        'wl-draft-send
        'wl-draft-kill
        'mail-send-hook))
  

; Fields in the e-mail header that I do not want to see (regexps)
(setq wl-message-ignored-field-list '(
    ".*Received:"
    ".*Path:"
    ".*Id:"
    "^References:"
    "^Replied:"
    "^Errors-To:"
    "^Lines:"
    "^Sender:"
    ".*Host:"
    "^Xref:"
    "^Content-Type:"
    "^Precedence:"
    "^Status:"
    "^X-VM-.*:"
    "^List-*"
    "^Authentication-Results*"
    "^X-*"
    "^Received-SPF*"
    "^DKIM-Signature:"
    "^DomainKey-Signature:"
    "^X-Mailman-Version:"))

; Fields in the e-mail header that I want to see even if they match the
; regex in wl-message-ignored-field-list
(setq wl-message-visible-field-list '(
    "^Dnas.*:"
    "^Message-Id:"
    "^X-Mailer:"
    "^X-Mailman-Version:"))

(setq wl-message-sort-field-list '(
    "^From:"
    "^To:"
    "^Cc:"
    "^Bcc:"
    "^Subject:"
    "^Date:"))
 
;; Enables auto-fill-mode in the draft buffer
(add-hook 'wl-mail-setup-hook 'auto-fill-mode)
  
; Use orgstruct++-mode in the draft buffer
;(add-hook 'mail-mode-hook 'turn-on-orgstruct)
;(add-hook 'mail-mode-hook 'turn-on-orgstruct++)
  
;; Set the key "f" to browse-url when I'm reading an E-mail. If instead of an url I have an HTML code I can simple select the code and hit "F"
(add-hook 'mime-view-mode-hook
            (lambda ()
              (local-set-key "f" 'browse-url)
              (local-set-key "F" 'browse-url-of-region)))

; Disable startup buffer.
(setq inhibit-startup-message t)

; Disable splitting large attachments.
(setq mime-edit-split-message nil)

(evil-set-initial-state 'wl-folder-mode 'emacs)
(evil-set-initial-state 'wl-summary-mode 'emacs)
(add-hook 'wl-folder-mode-hook  'turn-off-evil-mode)
(add-hook 'wl-summary-mode-hook 'turn-off-evil-mode)

; Start wanderlust.					;
(wl)
