# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd extendedglob nomatch notify
bindkey -e

# Lines added by compinstall.
zstyle :compinstall filename '/home/firegurafiku/.zshrc'
autoload -Uz compinit
compinit

# Allow calling my helper scripts by name.
PATH="$PATH:$HOME/PROJECTS/opensource/linux-misc-scripts"
PATH="$PATH:/opt/texlive-2014/bin/x86_64-linux"

# Locally installed packages for TeX goes here.
TEXINPUTS="$HOME/.tex:${TEXINPUTS:-.}"

INFOPATH="${INFOPATH:+$INFOPATH:}/opt/texlive-2014/texmf-dist/doc/info"
MANPATH="${MANPATH:+$MANPATH:}/opt/texlive-2014/texmf-dist/doc/man"

# A fucking simple configuration file tracking system.
HOMEGIT_REPO="$HOME/PROJECTS/private/config-betalibrae.git"
alias gith="git --git-dir='$HOMEGIT_REPO' --work-tree=/"

# 
alias ll='ls -l --color=auto'
alias l1='ls -1 --color=auto'
alias ys='yum search'
alias yp='yum provides'
alias yi='sudo yum install'
alias psgrep='sh -c "ps -fp \$(pgrep -d, \"\$1\")"'
alias tree='tree -N'
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

# Here NPS1 stands for "naked PS1" and isn't a built-in shell variable. I've
# defined it myself for PS1-PS2 alignment to operate properly.
PS1='%S%F{red}[%l]%f%s %F{green}%n@%m%f %B%#%b '
NPS1='[%l] %n@%m # '
RPS1='%B%F{green}(%~)%f%b'

module load mpi/openmpi-x86_64

# Hook function which gets executed right before shell prints prompt.
function precmd() {
    local expandedPrompt="$(print -P "$NPS1")"
    local promptLength="${#expandedPrompt}"
    PS2="> "
    PS2="$(printf "%${promptLength}s" "$PS2")"
}

