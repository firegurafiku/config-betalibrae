#!/bin/sh
#
#

#
#
xrandr --output DP1 --left-of LVDS1

# My custom keyboard layout with typographic characters.
# See https://bitbucket.org/firegurafiku/linux-typographic-layout
setxkbmode setxkbmap \
    -option grp:caps_toggle \
    -option grp_led:caps \
    -option lv3:ralt_switch \
    -layout "us+typo_us_es,ru(winkeys):2+typo_ru_uk" &

xxkb &
dunst &
nm-applet &
volumeicon &

feh --bg-center "$HOME/.i3/wallpaper.jpg"
xcompmgr &

