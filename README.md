Configuration files for my Linux
================================

This repository contains some text configuration files found on my system. They
may include, for example, files from `/etc`, `/usr/share` or home directory.
Repository made public in order to allow me to freely share my configuration
with others, so there is no chance you can find here any private data like SSH
or PGP keys.

N.B.:
Never change files in this repository using web interface, or next “git pull”
will be very painful as many files here are accessible to root user only.
